package com.example.preexamen;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

public class ReciboNominaActivity extends AppCompatActivity {
    // Declarar variables
    private EditText txtNumeroRecibo;
    private EditText txtNombre;
    private EditText txtHorasTrabajadasNormales;
    private EditText txtHorasTrabajadasExtra;
    private RadioButton rbAuxiliar;
    private RadioButton rbAlbañil;
    private RadioButton rbIngObra;

    private TextView lblUsuario;
    private EditText txtSubtotal;
    private EditText txtImpuesto;
    private EditText txtTotalPagar;

    private Button btnCalcular;
    private Button btnLimpiar;
    private Button btnRegresar;

    private ReciboNomina recibo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recibo_nomina);
        iniciarComponentes();

        // Obtener los datos del Main Activity
        Bundle datos = getIntent().getExtras();
        String nombre = datos.getString("nombre");
        lblUsuario.setText(nombre);

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calcular();
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                regresar();
            }
        });

    }

    private void iniciarComponentes() {
        txtNumeroRecibo = (EditText) findViewById(R.id.txtNumeroRecibo);
        txtNombre = (EditText) findViewById(R.id.txtNombre);
        txtHorasTrabajadasNormales = (EditText) findViewById(R.id.txtHorasTrabajadas);
        txtHorasTrabajadasExtra = (EditText) findViewById(R.id.txtHorasTrabajadasExtra);
        txtSubtotal = (EditText) findViewById(R.id.txtSubtotal);
        txtImpuesto = (EditText) findViewById(R.id.txtImpuesto);
        txtTotalPagar = (EditText) findViewById(R.id.txtTotalPagar);

        lblUsuario = (TextView) findViewById(R.id.lblUsuario);

        rbAuxiliar = (RadioButton) findViewById(R.id.radAuxiliar);
        rbAlbañil = (RadioButton) findViewById(R.id.radAlbañil);
        rbIngObra = (RadioButton) findViewById(R.id.radIngObra);

        btnCalcular = (Button) findViewById(R.id.btnCalcular);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnRegresar = (Button) findViewById(R.id.btnRegresar);

        recibo = new ReciboNomina(0, "", 0, 0, 0, 0);

        bloqueartextos();
    }

    private void calcular() {
        int puesto = 1;
        float horasNormales = 0;
        float horasExtras = 0;
        float subtotal = 0;
        float impuesto = 0;
        float total = 0;


        if (rbAuxiliar.isChecked()) {
            puesto = 1;
        } else if (rbAlbañil.isChecked()) {
            puesto = 2;
        } else if (rbIngObra.isChecked()) {
            puesto = 3;
        }

        if (txtNumeroRecibo.getText().toString().isEmpty()) {
            Toast.makeText(this, "Por favor, Ingrese su Número de Recibo", Toast.LENGTH_SHORT).show();
        } else if (txtNombre.getText().toString().isEmpty()) {
            Toast.makeText(this, "Por favor, Ingrese su Nombre", Toast.LENGTH_SHORT).show();
        } else if (txtHorasTrabajadasNormales.getText().toString().isEmpty() || txtHorasTrabajadasExtra.getText().toString().isEmpty()) {
            if (txtHorasTrabajadasNormales.getText().toString().isEmpty()) {
                Toast.makeText(this, "Ingrese la Cantidad de horas Trabajadas", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "Ingrese la Cantidad de Horas Extras", Toast.LENGTH_SHORT).show();
            }
        }

        if (!txtHorasTrabajadasNormales.getText().toString().isEmpty()) {
            horasNormales = Float.parseFloat(txtHorasTrabajadasNormales.getText().toString());
        }

        if (!txtHorasTrabajadasExtra.getText().toString().isEmpty()) {
            horasExtras = Float.parseFloat(txtHorasTrabajadasExtra.getText().toString());
        }

        if (!txtHorasTrabajadasNormales.getText().toString().isEmpty() && !txtHorasTrabajadasExtra.getText().toString().isEmpty() && puesto != 0) {
            subtotal = recibo.calcularSubtotal(puesto, horasNormales, horasExtras);
            impuesto = recibo.calcularImpuesto(puesto, horasNormales, horasExtras);
            total = recibo.calcularTotal(subtotal, impuesto);
            txtSubtotal.setText("" + subtotal);
            txtImpuesto.setText("" + impuesto);
            txtTotalPagar.setText("" + total);
            System.out.println("subtotal = " + subtotal);
            System.out.println("impuesto = " + impuesto);
            System.out.println("total = " + total);
        }
    }


    private void limpiar() {
        txtNumeroRecibo.setText("");
        txtNombre.setText("");
        txtHorasTrabajadasNormales.setText("");
        txtHorasTrabajadasExtra.setText("");
        txtSubtotal.setText("");
        txtImpuesto.setText("");
        txtTotalPagar.setText("");
    }

    private void regresar() {
        AlertDialog.Builder confirmar = new AlertDialog.Builder(this);
        confirmar.setTitle("Recibo de Nomina");
        confirmar.setMessage("Regresar al MainActivity");
        confirmar.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        confirmar.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // No hace nada
            }
        });
        confirmar.show();

    }

    //Bloquear edittexts de resultados
    private void bloqueartextos() {
        txtSubtotal.setFocusable(false);
        txtImpuesto.setFocusable(false);
        txtTotalPagar.setFocusable(false);
    }

}

