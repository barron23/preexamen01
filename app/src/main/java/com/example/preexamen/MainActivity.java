package com.example.preexamen;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private TextView lblNombreTrabajador;
    private EditText txtNombreTrabajador;

    private Button btnEntrar;
    private Button btnSalir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        iniciarComponentes();
        btnEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                entrar();
            }
        });

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                salir();
            }
        });
    }

    private void iniciarComponentes() {
        lblNombreTrabajador = (TextView) findViewById(R.id.lblNombreTrabajador);
        txtNombreTrabajador = (EditText) findViewById(R.id.txtNombreTrabajador);
        btnEntrar = (Button) findViewById(R.id.btnEntrar);
        btnSalir = (Button) findViewById(R.id.btnSalir);
    }

    private void entrar() {
        String strNombre;

        //Relacionar lo que introduce el usuario desde login con los valores del archivo values > string.xml
        strNombre = getApplicationContext().getResources().getString(R.string.nombre);

        //VALIDAR LOGIN
        if (strNombre.toString().equals(txtNombreTrabajador.getText().toString().trim())) {

            //Hacer el paquete para enviar la informacion
            Bundle bundle = new Bundle();
            bundle.putString("nombre", txtNombreTrabajador.getText().toString());

            //Hacer intent para llamar otra actividad
            //                                       origen              destino         de la peticion
            Intent intent = new Intent(MainActivity.this, ReciboNominaActivity.class);
            //enviando el paquete
            intent.putExtras(bundle);

            //Iniciar la actividad - esperando o no respuesta
            startActivity(intent);


        } else {
            Toast.makeText(this.getApplicationContext(), "El usuario o contraseña no son válido", Toast.LENGTH_SHORT).show();
        }
    }

    private void salir() {
        AlertDialog.Builder confirmar = new AlertDialog.Builder(this);
        confirmar.setTitle("PREEXAMEN");
        confirmar.setMessage("¿Desea cerrar la app?");
        confirmar.setPositiveButton("Confirmar", (dialog, which) -> finish());
        confirmar.setNegativeButton("Cancelar", (dialog, which) -> {});

        confirmar.show();
    }
}

